class HumanPlayer

  attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "where would you like to move? (row, col)"
    gets.chomp.split(',').map(&:to_i)
  end

  def display(board)
    row_0 = '0 |'
    (0..2).each do |col|
      row_0 << (board.empty?([0,col]) ? "  |" : " " + board[[0, col]].to_s + " |")
    end
    row_1 = '1 |'
    (0..2).each do |col|
      row_1 << (board.empty?([1,col]) ? "  |" : " " + board[[1,col]].to_s + " |")
    end
    row_2 = '2 |'
    (0..2).each do |col|
      row_2 << (board.empty?([2,col]) ? " |" : " " + board[[2,col]].to_s + " |")
   end
   puts "    0   1   2  "
   puts "  |-----------|"
   puts row_0
   puts "  |-----------|"
   puts row_1
   puts "  |-----------|"
   puts row_2
   puts "  |-----------|"
 end

end
